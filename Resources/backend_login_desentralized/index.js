const express = require("express");
const bodyParser = require("body-parser");
const ngrok = require("ngrok");
const decodeJWT = require("did-jwt").decodeJWT;
const { Credentials } = require("uport-credentials");
const transports = require("uport-transports").transport;
const message = require("uport-transports").message.util;

let endpoint = "";
const app = express();
app.use(bodyParser.json({ type: "*/*" }));

/*const credentials = new Credentials({
  appName: 'uPort Test 2019',
  did: tu_did,
  privateKey: tu_private
})*/

const credentials = new Credentials({
  appName: "Test uPort",
  did: "did:ethr:0x6fe49f4c47c9e0748722dfa8c7c7c2eb740350cc",
  privateKey: "83116ecc72e5c9d3f1e7c342eee27be0dfa41e84bcfd369d7a7d62f41e7423d8"
});

app.get("/", (req, res) => {
  credentials
    .createDisclosureRequest({
      requested: ["name"],
      notifications: true,
      callbackUrl: endpoint + "/callback"
    })
    .then(requestToken => {
      console.log(decodeJWT(requestToken)); //log request token to console
      const uri = message.paramsToQueryString(
        message.messageToURI(requestToken),
        { callback_type: "post" }
      );
      const qr = transports.ui.getImageDataURI(uri);
      res.send(`<div><img src="${qr}"/></div>`);
    });
});

app.post("/callback", (req, res) => {
  const jwt = req.body.access_token;
  console.log(jwt);
  credentials
    .authenticateDisclosureResponse(jwt)
    .then(credentials => {
      console.log(credentials);
      // Validate the information and apply authorization logic
    })
    .catch(err => {
      console.log(err);
    });
});

// run the app server and tunneling service
const server = app.listen(8088, () => {
  ngrok.connect(8088).then(ngrokUrl => {
    endpoint = ngrokUrl;
    console.log(`Login Service running, open at ${endpoint}`);
  });
});
