var READTHEDOCS_DATA = {
    project: "remix",
    version: "latest",
    language: "en",
    programming_language: "js",
    subprojects: {},
    canonical_url: "https://remix.readthedocs.io/en/latest/",
    theme: "sphinx_rtd_theme",
    builder: "sphinx",
    docroot: "/docs/",
    source_suffix: ".rst",
    api_host: "https://readthedocs.org",
    commit: "33f2dfb3",
    ad_free: false,

    global_analytics_code: 'UA-17997319-1',
    user_analytics_code: null
};

