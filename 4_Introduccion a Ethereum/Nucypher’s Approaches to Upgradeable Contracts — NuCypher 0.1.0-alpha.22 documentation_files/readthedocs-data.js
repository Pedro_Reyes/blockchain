var READTHEDOCS_DATA = {
    project: "nucypher",
    version: "latest",
    language: "en",
    programming_language: "py",
    subprojects: {},
    canonical_url: "https://nucypher.com/en/latest/",
    theme: "sphinx_rtd_theme",
    builder: "sphinx",
    docroot: "/docs/source/",
    source_suffix: ".rst",
    api_host: "https://readthedocs.org",
    commit: "23253c71",
    ad_free: false,

    global_analytics_code: 'UA-17997319-1',
    user_analytics_code: null
};

