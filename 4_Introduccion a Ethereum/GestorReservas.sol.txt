pragma solidity ^0.5.4;

contract GestorReservas {

    // Este sistema me permite gestionar la reserva de hasta 20 salas.
    // En cada casilla del array guarda la dirección del dueño actual de la reserva.
    address[20] public reserva;
    
    uint256[20] public hora_limite;

    // Reservar una sala en concreto durante un tiempo determinado (en segundos)
    function reservarSala(uint256 id_sala, uint256 tiempo_reserva) public returns (uint256) {
      
      // Validar los parametros de entrada
      require(id_sala >= 0 && id_sala < 20);
      require(tiempo_reserva <= 60 minutes);

      // Validar que la sala no esta reservada
      require(now > hora_limite[id_sala]);
      
      reserva[id_sala] = msg.sender;
      hora_limite[id_sala] = now + tiempo_reserva;
      return id_sala;
    }

    // Ver la información de una reserva
    function verReserva(uint256 id_sala) public view returns (address, uint256) {
      require(id_sala >= 0 && id_sala < 20);
      return (reserva[id_sala], hora_limite[id_sala]);
    }
    
    
}